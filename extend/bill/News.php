<?php
namespace bill;
/**
*+------------------
* Tpflow 单据适配类
*+------------------
* Copyright (c) 2006~2018 http://cojz8.cn All rights reserved.
*+------------------
* Author: guoguo(1838188896@qq.com)
*+------------------
*/

class News 
{
	protected $id; //对应单据编号
	protected $run_id; //运行中的流程id
	public function  __construct($id,$run_id='',$data=''){
		$this->id =$id;
		$this->run_id =$run_id;
    }
	//before 
	public function before($action=''){
		return '调用方法为：'.$action.'单据编号为：'.$this->id.'单据运行步骤为：'.$this->run_id;
	}
	public function after($action){
		return '调用方法为：'.$action.'单据编号为：'.$this->id.'单据运行步骤为：'.$this->run_id;
	}
}