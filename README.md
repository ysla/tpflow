# Tpflow V4.0 开发版

**欢迎使用 Tpflow 工作流引擎**

*   TpFlow工作流引擎是一套规范化的流程管理系统，基于业务而驱动系统生命力的一套引擎。 
*   彻底释放整个信息管理系统的的活力，让系统更具可用性，智能应用型，便捷设计性。 
*   Tpflow团队致力于打造中国最优秀的PHP工作流引擎。

![star](https://gitee.com/ntdgg/tpflow/badge/star.svg?theme=gvp "tpflow") ![fork](https://gitee.com/ntdgg/tpflow/badge/fork.svg?theme=gvp "tpflow") 

4.0开发计划：https://gitee.com/ntdgg/tpflow/board

### 主要特性

+ 基于  `<jsPlumb>` 可视化设计流程图
    + 支持可视化界面设计
    + 支持拖拽式流程绘制
    + 三布局便捷调整
    + 基于`workflow.3.0.js` `workflow.3.0.js ` 引擎
+ 超级强大的API 对接功能
    + `flowApi` 可支持工作流设计开发管理
    + `ProcessApi` 步骤管理API，可以对步骤进行管理、读取
    + `SuperApi ` 超级管理接口，对流程进行终止，代审
+ 完善的流引擎机制
    + 规范的命名空间，可拓展的集成化开发
    + 支持 直线式、会签式、转出式、同步审批式等多格式的工作流格式
+ 提供基于 `Thinkphp5.1.X` 的样例Demo
+ 提供完整的设计手册

>3.2版本更新说明
*   简化集成难度，优化安装部署
    * `View`视图文件集成于Tpflow核心目录，简化对 `Controller` `View` 的依赖
	* 使用路由进行定向流引擎功能
*   优化设计器、审批界面等页面

>3.1独有的新特性及功能

*   基于`<WordDb>`驱动的事务管理模块
    * 可以根据不同的需求对业务进行更广阔全面的调用
    * 可以使得流程步骤的设计更加灵活多变
*  新增事物接受者处理
    * 事务接受让环形审批流变得更加方便
*  全新的工作流设计界面  `步骤更清晰` `设计更简单`
    * 独立化步骤显示
    * TAB式步骤属性配置
    * 步骤审批、步骤模式更加清晰
 *  环形审批流模式
    * 解决以往A发起人->B审核人->C核准人->A发起人完结 的环型审批流 

### 在线文档

[看云文档](https://www.kancloud.cn/guowenbin/tpflow "安装手册")   [官方博客](https://www.cojz8.com/ "官方博客")

### 界面截图

![markdown](https://img.kancloud.cn/42/7a/427adc1dcc2ff3ffb52087b1cfde346b_1366x622.png)


### 官方教程连接

> https://www.cojz8.com/article/119   Tpflow3.1版本工作流设计-----教程一

> https://www.cojz8.com/article/120   工作流集成及API接口文档---教程二

> https://www.cojz8.com/article/121   Tpflow工作流引擎在事务方面的表现

> https://www.cojz8.com/article/123   工作流类型详解---教程三

> https://www.cojz8.com/article/124   工作流实战教程----教程四

~~~
设计器、源代码标注有版权的位置，未经许可，严禁删除及修改，违者将承担法律侵权责任！
~~~
### 相关链接
---

> 官方博客：https://www.cojz8.com/

> 演示站点：https://tpflow.cojz8.com/   

> 工作流手册：https://www.kancloud.cn/guowenbin/tpflow  赞助用户【VIP群】1062040103

> 视频教程：https://www.kancloud.cn/guowenbin/tpflow_video 【付费】视频教程

---

## 版权信息

Tpflow 遵循 MIT 开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018-2020 by Tpflow (http://cojz8.com)

All rights reserved。

~~~
对您有帮助的话，你可以在下方赞助我们，让我们更好的维护开发，谢谢！
特别声明：坚决打击网络诈骗行为，严禁将本插件集成在任何违法违规的程序上。
~~~
